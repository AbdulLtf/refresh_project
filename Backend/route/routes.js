const dataSurveiRoutes = require("../controller/data_survei");
module.exports = exports = function (server) {
  server.get("/api/getSurvei", dataSurveiRoutes.readSurveiAllHandler);
  server.post("/api/createSurvei", dataSurveiRoutes.createSurveiAllHandler);
  server.put("/api/editSurvei", dataSurveiRoutes.editSurveiAllHandler);
  server.put("/api/deleteSurvei", dataSurveiRoutes.deleteSurveiAllHandler);
};
