const pg = require("pg");
const DatabaseConnection = require("../config/config.json");
var DB = new pg.Pool(DatabaseConnection.config);
const Dtl_survei = {
  getAllSurveiData: (callback) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      client.query(`select*from data_survei order by id asc`, function (
        err,
        result
      ) {
        done();
        if (err) {
          data = err;
        } else {
          data = result.rows;
        }
        callback(data);
      });
    });
  },
  createSurveiHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `INSERT INTO data_survei (email,kesukarelaan,no_hp,status,kelurahan,kecamatan, kabupaten, fakultas, jurusan, gejala, resiko, faktor_resiko,transportasi,nama_lengkap,umur)
        values
        ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15)`,
        values: [
          docs.email,
          docs.kesukarelaan,
          docs.no_hp,
          docs.status,
          docs.kelurahan,
          docs.kecamatan,
          docs.kabupaten,
          docs.fakultas,
          docs.jurusan,
          docs.gejala,
          docs.resiko,
          docs.faktor_resiko,
          docs.transportasi,
          docs.nama_lengkap,
          docs.umur,
        ],
      };
      // console.log(JSON.stringify(query));
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil diinput";
        }
        callback(data);
      });
    });
  },
  editSurveiHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      // console.log(docs);
      const query = {
        text: `UPDATE data_survei
        SET email=$1,
        kesukarelaan = $2,
        no_hp=$3,
        status=$4,
        kelurahan=$5,
        kecamatan=$6,
        kabupaten=$7,
        fakultas=$8,
        jurusan=$9
        gejala=$10,
        resiko=$11,
        faktor_resiko=$12,
        transportasi=$13,
        nama_lengkap=$14,
        umur=$15,
        WHERE
        id = $16`,
        values: [
          docs.email,
          docs.kesukarelaan,
          docs.no_hp,
          docs.status,
          docs.kelurahan,
          docs.kecamatan,
          docs.kabupaten,
          docs.fakultas,
          docs.jurusan,
          docs.gejala,
          docs.resiko,
          docs.faktor_resiko,
          docs.transportasi,
          docs.nama_lengkap,
          docs.umur,
          docs.id,
        ],
      };
      console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil diubah ";
        }
        callback(data);
      });
    });
  },
  deleteSurveiHandlerData: (callback, docs) => {
    DB.connect(function (err, client, done) {
      var data = "";
      if (err) {
        data = err;
      }
      const query = {
        text: `DELETE FROM data_survei
        WHERE id=$1`,
        values: [docs.id],
      };
      console.log(query);
      client.query(query, function (err, result) {
        done();
        if (err) {
          data = err;
        } else {
          data = "data berhasil dihapus";
        }
        callback(data);
      });
    });
  },
};
module.exports = Dtl_survei;
