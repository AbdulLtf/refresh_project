const restify = require("restify");
const pg = require("pg");
const DatabaseConnection = require("./config/config.json");
const middleware = require("restify-cors-middleware");
var pool = new pg.Pool(DatabaseConnection.config);
pool.connect(function (err) {
  if (err) {
    console.log("not able to get connection" + err);
    process.exit;
  } else {
    console.log("[DATABASE] connected " + DatabaseConnection.config.database);
    const server = restify.createServer();
    const port = process.env.PORT || 3003;
    const cors = middleware({
      origins: ["*"],
      allowHeaders: ["Authorization"],
    });
    server.pre(cors.preflight);
    server.use(cors.actual);
    server.use(restify.plugins.queryParser());
    server.use(restify.plugins.bodyParser({ mapParams: false }));
    require("./route/routes")(server);
    server.listen(port, () => {
      console.log("[SERVER] running at port " + port);
    });
  }
});
