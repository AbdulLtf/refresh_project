const ResponseHelper = require("../helpers/responseHelper");
const Dtl_survei = require("../data_layer/dtl_survei");
const Data_Survei = {
  readSurveiAllHandler: (req, res, next) => {
    Dtl_survei.getAllSurveiData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    });
  },
  createSurveiAllHandler: (req, res, next) => {
    var docs = req.body;
    Dtl_survei.createSurveiHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  editSurveiAllHandler: (req, res, next) => {
    var docs = req.body;
    Dtl_survei.editSurveiHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
  },
  deleteSurveiAllHandler: (req, res, next) => {
    var docs = req.body;
    Dtl_survei.deleteSurveiHandlerData(function (items) {
      ResponseHelper.sendResponse(res, 200, items);
    }, docs);
    console.log(docs);
  },
};
module.exports = Data_Survei;
