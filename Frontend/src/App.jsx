import React from "react";
import Dashboard from "./component/Dashboard.jsx";

class App extends React.Component {
  render() {
    return <Dashboard />;
  }
}

export default App;
