import React from "react";
import { Switch, Route } from "react-router-dom";
import Header from "./Header";
import Sidebar from "./Sidebar";
import listsurvei from "./content/listSruvei";
import createsurvei from "./content/create";
import home from "./content/home";

class Dashboard extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Sidebar />

        <div class="content-wrapper">
          <div class="content-header"></div>
          <section class="content">
            <Switch>
              <Route path="/listsurvei" component={listsurvei} />
              <Route path="/createsurvei" component={createsurvei} />
              <Route path="/home" component={home} />
            </Switch>
          </section>
        </div>
      </div>
    );
  }
}
export default Dashboard;
