import React from "react";
import apiconfig from "../../config/api.json";
import axios from "axios";
class createSurvei extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formdata: {
        email: "",
        kesukarelaan: "",
        no_hp: "",
        nama_lengkap: "",
        status: "",
        kelurahan: "",
        kecamatan: "",
        kabupaten: "",
        fakultas: "",
        jurusan: "",
        gejala: [],
        resiko: [],
        faktor_resiko: [],
        transportasi: [],
      },
    };
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(e) {
    let tmp = this.state.formdata;
    tmp[e.target.name] = e.target.value;
    this.setState({
      formdata: tmp,
    });
  }
  render() {
    const { formdata } = this.state;
    return (
      <div>
        {JSON.stringify(formdata)}
        <div class="container">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <br></br>
              <li class="breadcrumb-item">
                <a href="/home">Home</a>
              </li>
              <li class="breadcrumb-item active">Create Survei</li>
            </ol>
            <h4>CREATE SURVEI</h4>
          </div>
          <div class="jumbotron">
            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Surveilans Coronavirus Disease 2019 (COVID-19) Sivitas
                  Akademika Universitas Jenderal Soedirman tahap II
                </h5>
                <p class="card-text">
                  Sebagai bagian dari upaya kesiapsiagaan dalam menghadapi
                  meluasnya penularan COVID 19, maka perlu dilakukan surveilans
                  terutama pada sivitas akademika Unsoed. Form Surveilans ini
                  diinisiasi oleh Tim Tanggap Pandemi COVID-19 Unsoed, data yang
                  dikumpulkan pada form ini akan dijaga kerahasiaannya dan
                  digunakan HANYA UNTUK KEPENTINGAN SURVEILANS. Untuk informasi
                  lebih lanjut anda dapat menghubungi nomor pusat layanan
                  COVID-19 Universitas Jenderal Soedirman melalui kontak telepon
                  (0281) 641233 atau WA 081 229 366 919
                </p>
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Email address<span style={{ color: "red" }}>*</span>
                </h5>
                <input
                  type="text"
                  name="email"
                  value={this.state.formdata.email}
                  onChange={this.changeHandler}
                  autoFocus
                  placeholder="Your Answer"
                  class="form-control"
                  style={{
                    borderTop: "none",
                    borderLeft: "none",
                    borderRight: "none",
                  }}
                />
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Form ini diisi berdasarkan kesukarelaan dan tidak terdapat
                  paksaan. Dengan berpartisipasi mengisi form ini, anda
                  menyatakan bersedia bahwa info kontak yang disediakan dapat
                  dihubungi lebih lanjut oleh Tim Tanggap Pandemi Covid-19
                  Unsoed apabila dianggap perlu. Semua informasi bersifat
                  rahasia, dikumpulkan serta dianalisis untuk kepentingan
                  kesiapsiagan Unsoed menghadapi perkembangan infeksi Covid-19.
                  Berikut ini saya menyatakan :{" "}
                  <span style={{ color: "red" }}>*</span>
                </h5>
              </div>
              <div
                class="form-check"
                style={{
                  marginLeft: "20px",
                  marginTop: "-15px",
                  paddingBottom: "10px",
                }}
              >
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Bersedia
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Menolak
                  </label>
                </div>
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Nama Lengkap Anda<span style={{ color: "red" }}>*</span>
                </h5>
                <input
                  type="text"
                  name="nama_lengkap"
                  value={this.state.formdata.nama_lengkap}
                  onChange={this.changeHandler}
                  autoFocus
                  placeholder="Your Answer"
                  class="form-control"
                  style={{
                    borderTop: "none",
                    borderLeft: "none",
                    borderRight: "none",
                  }}
                />
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Umur<span style={{ color: "red" }}>*</span>
                </h5>
                <input
                  type="text"
                  autoFocus
                  name="umur"
                  value={this.state.formdata.umur}
                  onChange={this.changeHandler}
                  placeholder="Your Answer"
                  class="form-control"
                  style={{
                    borderTop: "none",
                    borderLeft: "none",
                    borderRight: "none",
                  }}
                />
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Nomor Telepon yang Bisa dihubungi
                  <span style={{ color: "red" }}>*</span>
                </h5>
                <input
                  type="text"
                  autoFocus
                  placeholder="Your Answer"
                  class="form-control"
                  style={{
                    borderTop: "none",
                    borderLeft: "none",
                    borderRight: "none",
                  }}
                />
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Status
                  <span style={{ color: "red" }}>*</span>
                </h5>
              </div>
              <div
                class="form-check"
                style={{
                  marginLeft: "20px",
                  marginTop: "-15px",
                  paddingBottom: "10px",
                }}
              >
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Dosen
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Tenaga kependidikan
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Cleaning Service
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Security
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Mahasiswa
                  </label>
                </div>
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Alamat tempat tinggal saat ini (Kelurahan)
                  <span style={{ color: "red" }}>*</span>
                </h5>
                <input
                  type="text"
                  autoFocus
                  placeholder="Your Answer"
                  class="form-control"
                  style={{
                    borderTop: "none",
                    borderLeft: "none",
                    borderRight: "none",
                  }}
                />
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Alamat tempat tinggal saat ini (Kecamatan)
                  <span style={{ color: "red" }}>*</span>
                </h5>
                <input
                  type="text"
                  autoFocus
                  placeholder="Your Answer"
                  class="form-control"
                  style={{
                    borderTop: "none",
                    borderLeft: "none",
                    borderRight: "none",
                  }}
                />
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Alamat tempat tinggal saat ini (Kabupaten)
                  <span style={{ color: "red" }}>*</span>
                </h5>
                <input
                  type="text"
                  autoFocus
                  placeholder="Your Answer"
                  class="form-control"
                  style={{
                    borderTop: "none",
                    borderLeft: "none",
                    borderRight: "none",
                  }}
                />
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Fakultas
                  <span style={{ color: "red" }}>*</span>
                </h5>
              </div>
              <div
                class="form-check"
                style={{
                  marginLeft: "20px",
                  marginTop: "-15px",
                  paddingBottom: "10px",
                }}
              >
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Pertanian
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Biologi
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Ekonomi dan Bisnis
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Peternakan
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Hukum
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Ilmu Sosial dan Politik
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Kedokteran
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Teknik
                  </label>
                </div>
                <div>
                  <input
                    class="form-check-input"
                    type="radio"
                    name="exampleRadios"
                    id="exampleRadios2"
                    value="option2"
                  />
                  <label class="form-check-label" for="exampleRadios2">
                    Ilmu Kesehatan
                  </label>
                </div>
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Jurusan/ Departemen/ Program Studi/ Subunit kerja
                  <span style={{ color: "red" }}>*</span>
                </h5>
                <input
                  type="text"
                  autoFocus
                  placeholder="Your Answer"
                  class="form-control"
                  style={{
                    borderTop: "none",
                    borderLeft: "none",
                    borderRight: "none",
                  }}
                />
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Apakah anda sedang mengalami gejala seperti berikut dalam 14
                  hari terakhir (boleh memilih lebih dari satu) ?
                  <span style={{ color: "red" }}>*</span>
                </h5>
              </div>
              <div
                class="form-check"
                style={{
                  marginTop: "-15px",
                  paddingBottom: "10px",
                }}
              >
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Demam/riwayat demam dalam 14 hari terakhir
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Batuk/pilek/nyeri tenggorokan
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Sesak Napas
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Saya tidak mengalami keluhan seperti di atas
                  </label>
                </div>
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Apakah anda memiliki faktor risiko melakukan perjalanan ke
                  luar negeri atau kota-kota terjangkit di Indonesia dalam waktu
                  14 hari terakhir (boleh memilih lebih dari satu)?
                  <span style={{ color: "red" }}>*</span>
                </h5>
              </div>
              <div
                class="form-check"
                style={{
                  marginTop: "-15px",
                  paddingBottom: "10px",
                }}
              >
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Jakarta
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Bandung
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Yogyakarta
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Depok
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Tangerang
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Bogor
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Manado
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Pontianak
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Saya tidak memiliki riwayat perjalanan ke luar negeri maupun
                    kota yang terjangkit
                  </label>
                </div>
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Apakah anda memiliki riwayat paparan faktor risiko berikut
                  dalam 14 hari terakhir? (boleh memilih lebih dari satu) *
                  <span style={{ color: "red" }}>*</span>
                </h5>
              </div>
              <div
                class="form-check"
                style={{
                  marginTop: "-15px",
                  paddingBottom: "10px",
                }}
              >
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Riwayat kontak erat dengan pasien yang terkonfirmasi atau
                    suspect/terduga terkena COVID-19
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Bekerja atau mengunjungi fasilitas kesehatan yang
                    berhubungan dengan pasien yang terkonfirmasi atau
                    suspect/terduga terkena COVID-19
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Memiliki demam ({">"} 38 derajat C) atau ada riwayat demam
                    dalam 14 hari terakhir, memiliki riwayat perjalanan ke luar
                    negeri atau kontak dengan orang yang memiliki riwayat
                    perjalanan ke luar negeri
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Saya tidak memiliki faktor risiko seperti di atas
                  </label>
                </div>
              </div>
            </div>

            <div class="card" style={{ width: "100%" }}>
              <div class="card-body">
                <h5 class="card-title" style={{ fontWeight: "bold" }}>
                  Transportasi umum yang biasa digunakan dalam kurun waktu 14
                  hari terakhir (boleh memilih lebih dari satu)
                  <span style={{ color: "red" }}>*</span>
                </h5>
              </div>
              <div
                class="form-check"
                style={{
                  marginTop: "-15px",
                  paddingBottom: "10px",
                }}
              >
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Kendaraan pribadi
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Kereta api
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Bus
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Angkot
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Taksi
                  </label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    value=""
                    id="defaultCheck1"
                  />
                  <label class="form-check-label" for="defaultCheck1">
                    Ojek Online
                  </label>
                </div>
              </div>
            </div>

            <button
              class="btn btn-primary btn-lg float-right"
              onClick={this.showHandler}
            >
              Kirim Jawaban Anda
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default createSurvei;
