import React from "react";
import apiconfig from "../../config/api.json";
import axios from "axios";
import { Link } from "react-router-dom";
import DeleteSurvei from "./delete";

class listSurvei extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      survei: [],
      currentSurvei: {},
      deleteSurvei: false,
    };
    this.deleteModalHandler = this.deleteModalHandler.bind(this);
    this.closeModalHandler = this.closeModalHandler.bind(this);
    // this.viewHandler = this.viewHandler.bind(this);
  }
  closeModalHandler() {
    this.setState({
      deleteSurvei: false,
    });
    this.getListSurvei();
  }

  deleteModalHandler(id) {
    let tmp = {};
    this.state.survei.map((row) => {
      if (id == row.id) {
        tmp = row;
      }
    });
    this.setState({
      currentSurvei: tmp,
      deleteSurvei: true,
    });
  }
  getListSurvei() {
    let option = {
      url: apiconfig.BASE_URL + apiconfig.ENDPOINTS.GETDATA,
      method: "get",
    };
    axios(option)
      .then((response) => {
        this.setState({
          survei: response.data.message,
        });
      })
      .catch((error) => {
        alert(error);
      });
  }
  componentDidMount() {
    this.getListSurvei();
  }
  render() {
    return (
      <div>
        <div class="container">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <br></br>
              <li class="breadcrumb-item">
                <a href="/home">Home</a>
              </li>
              <li class="breadcrumb-item active">List Survei</li>
            </ol>
            <h4>LIST SURVEI</h4>
          </div>
          <DeleteSurvei
            delete={this.state.deleteSurvei}
            closeModalHandler={this.closeModalHandler}
            survei={this.state.currentSurvei}
          />
          <div class="jumbotron">
            <table id="mytable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>Email</th>
                  <th>Nama Lengkap</th>
                  <th>Umur</th>
                  <th>Transportasi</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {this.state.survei.map((row, x) => (
                  <tr>
                    <td>{x + 1}</td>
                    <td>{row.email}</td>
                    <td>{row.nama_lengkap}</td>
                    <td>{row.umur}</td>
                    <td>{row.transportasi}</td>
                    <td>
                      <Link to="#">
                        <span
                          onClick={() => {
                            this.editModalHandler(row.id);
                          }}
                          className="fa fa-edit"
                          style={{ fontSize: "18px", paddingRight: "30px" }}
                        ></span>
                        <span
                          onClick={() => {
                            this.deleteModalHandler(row.id);
                          }}
                          className="fa fa-trash"
                          style={{ fontSize: "18px", paddingRight: "30px" }}
                        ></span>
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
export default listSurvei;
