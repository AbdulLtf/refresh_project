import React from 'react'
import apiconfig from '../../../configs/api.configs.json'
import axios from 'axios'
import {Link} from 'react-router-dom'
import ViewMahasiswa from './viewMahasiswa'
import DeleteMahasiswa from './deleteMahasiswa'
import EditMahasiswa from './editMahasiswa'
import CreateMahasiswa from './createMahasiswa'
import { MDBDataTable } from 'mdbreact';
// import alert from 'reactstrap'
class listmahasiswa extends React.Component {
    constructor(props){
        super(props)
        this.state={
            mahasiswa:[],
            currentMahasiswa:{},
            viewMahasiswa:false,
            deleteMahasiswa:false,
            editMahasiswa:false,
            showCreateMahasiswa:false
        }
        this.showHandler=this.showHandler.bind(this)
        this.viewModalHandler = this.viewModalHandler.bind(this)
        this.editModalHandler = this.editModalHandler.bind(this)
        this.deleteModalHandler = this.deleteModalHandler.bind(this)
        this.closeModalHandler = this.closeModalHandler.bind(this)
    }
    closeModalHandler(){
        this.setState({
            viewMahasiswa:false,
            editMahasiswa:false,
            deleteMahasiswa:false,
            showCreateMahasiswa:false
        })
        this.getListMahasiswa()
        
    }

    deleteModalHandler(kdmhs){
        let tmp ={}
        this.state.mahasiswa.map((row)=>{
            if(kdmhs == row.kd_mhs){
                tmp = row
            }
        })
        this.setState({
            currentMahasiswa: tmp,
            deleteMahasiswa:true
        })
    }
    editModalHandler(kdmhs){
        let tmp ={}
        this.state.mahasiswa.map((row)=>{
            if(kdmhs == row.kd_mhs){
                this.setState({
                    currentMahasiswa: row,
                    editMahasiswa:true
                })
            }
        })
        
    }
    
    viewModalHandler(kdmhs){
        let tmp ={}
        this.state.mahasiswa.map((row)=>{
            if(kdmhs == row.kd_mhs){
                tmp = row
            }
        })
        this.setState({
            currentMahasiswa: tmp,
            viewMahasiswa:true
        })
    }
    
    showHandler(){
        this.setState({showCreateMahasiswa:true})
    }
    getListMahasiswa(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option={
            url:apiconfig.BASE_URL+apiconfig.ENDPOINTS.MAHASISWA,
            method:"get",
            headers:{
                "Authorization":token
            }
        }
        axios(option)
        .then((response)=>{
            let tmp=[]
            response.data.message.map((row,x)=>{
                let c = <Link to='#'>
                <span onClick={()=>{this.viewModalHandler(row.kd_mhs)}} className="fa fa-search" style={{fontSize:'18px',paddingRight:'30px'}}></span>
                <span onClick={()=>{this.editModalHandler(row.kd_mhs)}} className="fa fa-edit" style={{fontSize:'18px',paddingRight:'30px'}}></span>
                <span onClick={()=>{this.deleteModalHandler(row.kd_mhs)}} className="fa fa-trash" style={{fontSize:'18px',paddingRight:'30px'}}></span>
                </Link>
                tmp.push({"nomor":x+1,"kd_mhs":row.kd_mhs,"nm_mhs":row.nm_mhs,"j_kel":row.j_kel,"alamat":row.alamat,"kd_jurusan":row.kd_jurusan,"action":c})
            })

            this.setState({
                mahasiswa:tmp
                // mahasiswa:response.data.message
            })
        })
        .catch((error)=>{
            alert(error)
        })
    }
    componentDidMount(){
        this.getListMahasiswa()
    }
    
            
    render(){
        const data = {
            columns: [
              {
                label: 'Nomor',
                field: 'nomor',
                sort: 'asc',
                width: 100
              },
              {
                label: 'Kode Mahasiswa',
                field: 'kd_mhs',
                sort: 'asc',
                width: 270
              },
              {
                label: 'Nama Mahasiswa',
                field: 'nm_mhs',
                sort: 'asc',
                width: 200
              },
              {
                label: 'Jenis Kelamin',
                field: 'j_kel',
                sort: 'asc',
                width: 100
              },
              {
                label: 'Alamat',
                field: 'alamat',
                sort: 'asc',
                width: 150
              },
              {
                label: 'KJurusan',
                field: 'kd_jurusan',
                sort: 'asc',
                width: 100
              },
              {
                label: 'Action',
                field: 'action',
                sort: 'asc',
                width: 100
              }
            ],
            rows: this.state.mahasiswa 
        }
        return(
           <div>
               <div class="container">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                        <br></br>
                        <li class="breadcrumb-item"><a href="/home">Home</a></li>
                        <li class="breadcrumb-item active">List mahasiswa</li>
                        </ol>
                        <h4>LIST MAHASISWA</h4> 
                    </div>
                    
                    <CreateMahasiswa
                    create ={this.state.showCreateMahasiswa}
                    closeModalHandler = {this.closeModalHandler}
                    />
                    <ViewMahasiswa
                    view ={this.state.viewMahasiswa}
                    closeModalHandler = {this.closeModalHandler}
                    mahasiswa = {this.state.currentMahasiswa}
                    />
                    <EditMahasiswa
                    edit ={this.state.editMahasiswa}
                    closeModalHandler = {this.closeModalHandler}
                    mahasiswa = {this.state.currentMahasiswa}
                    />
                    <DeleteMahasiswa
                    delete={this.state.deleteMahasiswa}
                    closeModalHandler = {this.closeModalHandler}
                    mahasiswa = {this.state.currentMahasiswa}
                    />
                    
                    <div class="jumbotron">
                    <p class="lead">INI ADALAH LIST MAHASISWA</p>
                        <MDBDataTable
                        striped
                        borderless
                        hover
                        data={data}
                        />
                    <button class="btn btn-primary btn-lg float-right" onClick={this.showHandler}>Add</button>
                    </div>
               </div>
           </div> 
        )
    }
}
export default listmahasiswa