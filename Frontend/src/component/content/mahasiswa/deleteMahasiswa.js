import apiconfig from '../../../configs/api.configs.json'
import React from 'react'
import {Modal, ModalBody, ModalFooter, ModalHeader, Button} from 'reactstrap'
import axios from 'axios'

class DeleteMahasiswa extends React.Component{
    constructor(props){
        super(props)
        this.deleteHandler = this.deleteHandler.bind(this)
    }
    deleteHandler(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.MAHASISWA,  
            method: "delete",
            headers:{
                "Authorization": token
            },
            data:this.props.mahasiswa.kd_mhs

        }
        axios(option)
        .then((response)=> {
            // console.log(this.state.formdata)
            if(response.data.code === 200){
                alert('success')
            }else{
                alert('failed')
            }
        })
        .catch((error)=> {
            console.log(error);
        })
        this.props.closeModalHandler()
    }
    render(){
        return(
            <Modal isOpen={this.props.delete} className={this.props.className}>
                <ModalHeader>View Mahasiswa</ModalHeader>
                <ModalBody>
                    <h1>DELETE DATA???</h1>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.deleteHandler}>YAA</Button>
                    <Button color="danger" onClick={this.props.closeModalHandler}>Close</Button>
                </ModalFooter>
            </Modal>
        )
    }
}
export default DeleteMahasiswa