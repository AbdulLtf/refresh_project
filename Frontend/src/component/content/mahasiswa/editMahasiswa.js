import apiconfig from '../../../configs/api.configs.json'
import React from 'react'
import {Modal, ModalBody, ModalFooter, ModalHeader, Button} from 'reactstrap'
import axios from 'axios'

class EditMahasiswa extends React.Component{
    constructor (props){
        super(props)
        //let userdata=""
        //if(localStorage.getItem(apiconfig.LS.TOKEN))!=null{
        let userdata = JSON.parse(localStorage.getItem(apiconfig.LS.USERDATA))
    //}
        this.state={
            formdata:{
                kd_mhs:'',
                nm_mhs:'',
                jk:'',
                alamat:'',
                update_by:userdata.username
            }
        }
        this.submitHandler = this.submitHandler.bind(this)
        this.changeHandler= this.changeHandler.bind(this)
    }

    componentWillReceiveProps(newProps){  //methode react
    // alert (JSON.stringify(newProps.mahasiswa))
        this.setState({
            formdata : newProps.mahasiswa
        
        })
        // newproops mahasiswatest
    }

    changeHandler(e){
        let tmp = this.state.formdata
        tmp [e.target.name]=e.target.value
        this.setState({
            formdata:tmp
        })
    }

    submitHandler(){
        let token = localStorage.getItem(apiconfig.LS.TOKEN)
        let option = {
            url: apiconfig.BASE_URL+apiconfig.ENDPOINTS.MAHASISWA,  
            method: "put",
            headers:{
                "Authorization": token,
                "Content-Type": "application/json"
            },
            data: this.state.formdata
        }

        axios(option)
        .then((response)=> {
            // console.log(this.state.formdata)
            if(response.data.code === 200){
                alert('data sudah diubah')
                this.props.history.push('/Dashboard')
            }else{
                alert(response.data.message)
            }
        })
        .catch((error)=> {
            console.log(error);
        })
        this.props.closeModalHandler()
    }

    render(){
        //console.log(this.state.formdata)
        return(
            <Modal isOpen={this.props.edit} className={this.props.className}>
            <ModalHeader> Edit Mahasiswa</ModalHeader>
            <ModalBody>

                <form class="form-inline">
                    <div class ="input-group mb-3 input-group-sm">
                        <label for= "text"> Kode Mahasiswa : </label>
                        <input type="text" class="form-control" readOnly
                        name="kd_mhs"
                        value ={this.state.formdata.kd_mhs}
                        onChange = {this.changeHandler}/>
                    </div>
                    <div class ="input-group mb-3 input-group-sm">
                    <label for= "text"> name Mahasiswa : </label>
                        <input type="text" class="form-control" //readOnly
                        name="nm_mhs"
                        value ={this.state.formdata.nm_mhs}
                        onChange = {this.changeHandler}/>
                    </div>
                </form>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={this.submitHandler}>save</Button>
                <Button color="warning" onClick={this.props.closeModalHandler}>Cancel</Button>
            </ModalFooter>
            </Modal>
            

        )
    }

}

export default EditMahasiswa