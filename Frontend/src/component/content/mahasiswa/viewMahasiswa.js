import React from 'react'
import {Modal,ModalBody,ModalFooter,ModalHeader,Button} from 'reactstrap'

class ViewMahasiswa extends React.Component{
    render(){
        return(
            <Modal isOpen={this.props.view} className={this.props.className}>
                <ModalHeader>View Mahasiswa</ModalHeader>
                <ModalBody>
                    <form class="form-inline">
                        <div class="input-group mb-3 input-group-sm" >
                            <div>

                            <p class="mr-3" >Kode Mahasiswa {this.props.mahasiswa.kd_mhs}</p>
                            <p class="mr-3">Nama Mahasiswa {this.props.mahasiswa.nm_mhs}</p>
                            </div>
                            <div>

                            <p class="mr-3">Jenis Kelamin {this.props.mahasiswa.j_kel}</p>
                            <p class="mr-3">Alamat {this.props.mahasiswa.alamat}</p>
                            </div>
                        </div>
                    </form>
                </ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={this.props.closeModalHandler}>Close</Button>
                </ModalFooter>
            </Modal>
        )
    }
}
export default ViewMahasiswa