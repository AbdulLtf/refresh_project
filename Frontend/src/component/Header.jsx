import React from "react";

class Header extends React.Component {
  render() {
    return (
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#">
              <i class="fas fa-bars"></i>
            </a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="/home" class="nav-link">
              Home
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Header;
