--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

-- Started on 2020-07-09 17:04:27

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 16887)
-- Name: data_survei; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.data_survei (
    id bigint NOT NULL,
    email character varying(100) NOT NULL,
    kesukarelaan bigint NOT NULL,
    no_hp character varying(20) NOT NULL,
    status character varying(50) NOT NULL,
    kelurahan character varying(50) NOT NULL,
    kecamatan character varying(50) NOT NULL,
    kabupaten character varying(50) NOT NULL,
    fakultas character varying(50) NOT NULL,
    jurusan character varying(50) NOT NULL,
    gejala character varying(200) NOT NULL,
    resiko character varying(1000) NOT NULL,
    faktor_resiko character varying(1000) NOT NULL,
    transportasi character varying(1000) NOT NULL,
    nama_lengkap character varying(100) NOT NULL,
    umur bigint NOT NULL
);


ALTER TABLE public.data_survei OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16885)
-- Name: data_survei_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.data_survei ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.data_survei_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999
    CACHE 1
    CYCLE
);


--
-- TOC entry 2816 (class 0 OID 16887)
-- Dependencies: 203
-- Data for Name: data_survei; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.data_survei (id, email, kesukarelaan, no_hp, status, kelurahan, kecamatan, kabupaten, fakultas, jurusan, gejala, resiko, faktor_resiko, transportasi, nama_lengkap, umur) FROM stdin;
1	latieff@gmail.com	1	02394920239	mahasiswa	cipinang muara	jatinegara	jakarta timur	teknik	informatika	Saya tidak mengalami keluhan seperti di atas	Bandung	Riwayat kontak erat dengan pasien yang terkonfirmasi atau suspect/terduga terkena COVID-19	Angkot	Latief Abdul	20
2	abdul@gmail.com	1	02394920239	mahasiswa	cipinang muara	jatinegara	jakarta timur	teknik	informatika	Saya tidak mengalami keluhan seperti di atas	Jakarta	Riwayat kontak erat dengan pasien yang terkonfirmasi atau suspect/terduga terkena COVID-19	Bus,Angkot	Abdul Latief	22
\.


--
-- TOC entry 2822 (class 0 OID 0)
-- Dependencies: 202
-- Name: data_survei_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.data_survei_id_seq', 4, true);


--
-- TOC entry 2688 (class 2606 OID 16894)
-- Name: data_survei data_survei_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.data_survei
    ADD CONSTRAINT data_survei_pkey PRIMARY KEY (id);


-- Completed on 2020-07-09 17:04:29

--
-- PostgreSQL database dump complete
--

